## About

A binding to the [STFL] library.  Covers the entire API, including the
nonsensical bits.

## Examples

Run `chicken-install -n` to install their dependencies and build them.

## Docs

TBD.  Refer to the library's homepage for the time being.

[STFL]: http://www.clifford.at/stfl/
